package com.xymob.whistlewiki.data.ws;

import android.content.Context;

import com.xymob.whistlewiki.config.LogConfig;
import com.xymob.whistlewiki.config.WSConfig;
import com.xymob.whistlewiki.model.UserRegistrationResponse;

import java.io.IOException;
import java.util.Map;

import okhttp3.Interceptor;
import okhttp3.OkHttpClient;
import okhttp3.Request;
import okhttp3.Response;
import okhttp3.logging.HttpLoggingInterceptor;
import retrofit2.Call;
import retrofit2.Retrofit;
import retrofit2.converter.gson.GsonConverterFactory;
import retrofit2.http.FieldMap;
import retrofit2.http.FormUrlEncoded;
import retrofit2.http.POST;

/**
 * Created by Admin on 8/4/2016.
 */
public class RequestManager {
    private final String TAG = "RequestManager";

    public static final String BASE_SERVER = "";
    public static final String BASE_SERVER_HTTPS = "";
    private static RequestManager mRequestManager = null;
    private boolean mIsContentTypeNeeded = true;
    private String mContentType;
    private Retrofit mRestAdapter;

    private RegisterUserService mRegisterUserService;

    /*
	 * Private constructor to avoid creating direct instance.
	 */
    private RequestManager() {
        mContentType = "application/json"; // Default content type.
    }

    public static RequestManager getInstance() {
        if (mRequestManager == null) {
            mRequestManager = new RequestManager();
        }
        return mRequestManager;
    }

    public Retrofit getRestAdapter(final Context context) {
        LogConfig.logv(TAG, "getRestAdapter()");
        return getRestAdapter(context, false, true);
    }

    public Retrofit getRestAdapter(final Context context, final boolean isForHttps,
                                   final boolean isGZipEnable) {
        LogConfig.logv(TAG, "getRestAdapter()");
        if (mRestAdapter == null) {
            LogConfig.logv(TAG, "getRestAdapter() create adapter");
            OkHttpClient.Builder httpClient = new OkHttpClient.Builder();
            httpClient.addInterceptor(new Interceptor() {
                @Override
                public Response intercept(Interceptor.Chain chain) throws IOException {
                    Request original = chain.request();

                    Request.Builder requestBuilder = original.newBuilder();
                    if (isGZipEnable) {
                        requestBuilder.header(WSConfig.HEADER_ACCEPT_ENCODING, "gzip");
                    }
                    if (mIsContentTypeNeeded) {
                        requestBuilder.header(WSConfig.HEADER_CONTENT_TYPE, mContentType);
                        mIsContentTypeNeeded = false;
                        mContentType = "application/json"; // Default content type.
                    }
                    requestBuilder.method(original.method(), original.body());
                    Request request = requestBuilder.build();
                    return chain.proceed(request);
                }
            });

            String baseUrl;
            if (isForHttps) {
                baseUrl = BASE_SERVER_HTTPS;
            } else {
                baseUrl = BASE_SERVER;
            }
            httpClient.addInterceptor(new HttpLoggingInterceptor().setLevel(HttpLoggingInterceptor.Level.BODY));
            OkHttpClient client = httpClient.build();
            mRestAdapter = new Retrofit.Builder()
                    .baseUrl(baseUrl)
                    .addConverterFactory(GsonConverterFactory.create())
                    .client(client)
                    .build();
        }
        return mRestAdapter;
    }

    public interface RegisterUserService {
        @FormUrlEncoded
        @POST("")
        Call<UserRegistrationResponse> registerUser(@FieldMap Map<String, String> fieldMap);
    }

    public RegisterUserService getRegisterUserService(Context context) {
        LogConfig.logv(TAG, "getRegisterUserService()");
        if (mRegisterUserService == null) {
            LogConfig.logv(TAG, "getRegisterUserService() create new ");
            mRegisterUserService = getRestAdapter(context, false, false).create(RegisterUserService.class);
        }
        return mRegisterUserService;
    }

    public void resetRegisterUserService() {
        LogConfig.logv(TAG, "resetUserLoginService()");
        mRegisterUserService = null;
    }
}
