package com.xymob.whistlewiki.config;

/**
 * Created by Admin on 8/4/2016.
 */
public interface WSConfig {
    public final String HEADER_ACCEPT_ENCODING = "Accept-Encoding";
    public final String HEADER_CONTENT_TYPE = "Content-Type";
}
