package com.xymob.whistlewiki.config;

import android.content.Context;
import android.content.SharedPreferences;
import android.os.Build;

import com.xymob.whistlewiki.R;
import com.xymob.whistlewiki.application.App;

/**
 * Created by Admin on 8/4/2016.
 */
public class PreferenceConfig {
    private static String TAG = "PreferenceConfig";
    private static SharedPreferences mSharedPreferences;

    public interface PreferenceConstants {
        String IS_REGISTERED = "IS_REGISTERED";
    }
    /** Method to get the Singleton instance of {@link SharedPreferences}. */
    public static SharedPreferences getInstance() {
        Context context = App.mContext;
        if (mSharedPreferences == null) {
            mSharedPreferences = context.getSharedPreferences(
                    context.getResources().getString(R.string.preferences_name), Context.MODE_PRIVATE);
        }
        return mSharedPreferences;
    }

    /** Method to set user register info. */
    public static void setUserRegisterStatus( final boolean status) {
        PreferenceConfig.getInstance().edit().putBoolean(
                PreferenceConstants.IS_REGISTERED,
                status).apply();
    }

    /** Method to get user register info. */
    public static boolean getUserRegisterStatus() {
        return PreferenceConfig.getInstance()
                .getBoolean(PreferenceConstants.IS_REGISTERED, false);
    }
}
