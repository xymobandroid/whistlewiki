package com.xymob.whistlewiki.activity;

import android.os.Bundle;

import com.xymob.whistlewiki.R;
import com.xymob.whistlewiki.config.PreferenceConfig;

/**
 * Created by Admin on 8/4/2016.
 */
public class RegistrationActivity extends BaseActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_registration);
        PreferenceConfig.setUserRegisterStatus(true);
    }

    @Override
    protected void onResume() {
        super.onResume();
    }
}
