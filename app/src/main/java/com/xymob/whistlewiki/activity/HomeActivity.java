package com.xymob.whistlewiki.activity;

import android.os.Bundle;

import com.xymob.whistlewiki.R;

/**
 * Created by Admin on 8/4/2016.
 */
public class HomeActivity extends BaseActivity {
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_home);
    }

    @Override
    protected void onResume() {
        super.onResume();
    }
}
