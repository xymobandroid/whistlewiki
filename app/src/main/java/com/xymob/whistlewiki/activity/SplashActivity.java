package com.xymob.whistlewiki.activity;

import android.content.Intent;
import android.os.Bundle;
import android.os.Handler;

import com.xymob.whistlewiki.config.LogConfig;
import com.xymob.whistlewiki.R;
import com.xymob.whistlewiki.config.PreferenceConfig;

/**
 * Created by Admin on 8/4/2016.
 */
public class SplashActivity extends BaseActivity {
    private Handler mHandler;
    final int SPLASH_TIME_OUT = 2400;
    final String TAG = "SplashActivity";

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        LogConfig.logd(TAG, "onCreate");
        setContentView(R.layout.activity_splash);
        mHandler = new Handler();
    }

    @Override
    protected void onResume() {
        super.onResume();
        LogConfig.logd(TAG, "onResume");
        mHandler.postDelayed(new Runnable() {

            @Override
            public void run() {
                if (PreferenceConfig.getUserRegisterStatus()) {
                    //User is registered then launch Login screen.
                    startActivity(new Intent(SplashActivity.this, LoginActivity.class));
                } else {
                    //User is not registered then launch home screen.
                    startActivity(new Intent(SplashActivity.this, HomeActivity.class));
                }
                finish();
            }
        }, SPLASH_TIME_OUT);
    }
}
