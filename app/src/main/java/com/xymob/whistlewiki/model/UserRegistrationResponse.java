package com.xymob.whistlewiki.model;

/**
 * Created by Admin on 8/4/2016.
 */
public class UserRegistrationResponse extends CommonResponse {
    public boolean userExists;
    public String secretId;
    public String zipCode;
    public String uid;
    public boolean locationAlert;
    public boolean weekendAlert;
    public String email;
}